import React, { useState } from "react";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import CancelIcon from "@mui/icons-material/Cancel";
import SaveIcon from "@mui/icons-material/Save";
import { Input } from "@mui/material";

function ListItem({ task, onDelete, onUpdate }) {
  let [editable, setEditable] = useState(false);
  let [innerName, setInnerName] = useState(task?.name || "");
  let toggleEditable = () => setEditable(!editable);
  const name = task.name;
  const id = task.id;
  const isImportant = name.includes("!");
  return (
    <div>
      <div
        style={{
          display: "flex",
          width: "250px",
          backgroundColor: isImportant == true ? "rgb(185, 61, 32)" : "rgb(26, 130, 181)",
          margin: "12px",
          padding: "12px",
          position: "relative",
        }}
      >
        {editable ? (
          <Input
            value={innerName}
            onChange={(e) => setInnerName(e.target.value)}
          />
        ) : (
          <p>{name}</p>
        )}
        {editable ? (
          <div style={{ display: "flex", position: "absolute", right: 0 }}>
            <IconButton aria-label="cancel" onClick={toggleEditable}>
              <CancelIcon />
            </IconButton>
            <IconButton
              aria-label="save"
              onClick={() => {
                onUpdate(id, { ...task, name: innerName });
                toggleEditable();
              }}
            >
              <SaveIcon />
            </IconButton>
          </div>
        ) : (
          <div
            style={{
              display: "flex",
              position: "absolute",
              right: 0,
              top: 0,
              bottom: 0,
            }}
          >
            <IconButton aria-label="edit" onClick={toggleEditable}>
              <EditIcon />
            </IconButton>
            <IconButton aria-label="delete" onClick={() => onDelete(task.id)}>
              <DeleteIcon />
            </IconButton>
          </div>
        )}
      </div>
    </div>
  );
}

export default ListItem;
