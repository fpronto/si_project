import React, { useState } from "react";
import { v1 } from "uuid";
import IconButton from "@mui/material/IconButton";
import ListItem from "../ListItem";
import AddIcon from "@mui/icons-material/Add";

let List = () => {
  let [taskList, setTaskList] = useState([]);

  let handleDelete = (id) => {
    let newTaskList = taskList.filter((task) => task.id !== id);
    setTaskList(newTaskList);
  };

  let handleUpdate = (id, newTask) => {
    let newTaskList = taskList.map((task) => (task.id === id ? newTask : task));
    setTaskList(newTaskList);
  };

  let handleNewTask = () => {
    debugger;
    let newTaskList = [...taskList, { id: v1(), name: "New Task" }];
    setTaskList(newTaskList);
  };
  const hasImportantTask = taskList.some((task) => task.name.includes("!"));

  return (
    <div
      style={{
        display: "flex",
        width: "500px",
        heigth: "500px",
        flexDirection: "column",
        backgroundColor: "grey",
        borderRadius: "12px",
        padding: "24px",
        maxHeight: "800px",
        minHeight: "800px",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          position: "relative",
          flexDirection:"column"
        }}
      >
        <h1>Task List</h1>
        {hasImportantTask == true ? <h2><b>Some of them are important</b></h2>: null}

        <IconButton
          style={{ position: "absolute", right: "12px" }}
          aria-label="add"
          onClick={handleNewTask}
        >
          <AddIcon />
        </IconButton>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          maxHeight: "650px",
          overflow: "auto",
          position: "relative",
        }}
      >
        {taskList.map((task) => (
          <ListItem
            key={task.id}
            task={task}
            onUpdate={handleUpdate}
            onDelete={handleDelete}
          />
        ))}
      </div>
    </div>
  );
};

export default List;
