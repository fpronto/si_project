import { List } from "./components";
import "./App.css";

const App = () => (
  <div
    style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "100%",
    }}
  >
    <List />
  </div>
);

export default App;
